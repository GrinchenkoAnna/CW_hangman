#ifndef GAME_PROCESS_H
#define GAME_PROCESS_H

int game_process(unsigned int size, unsigned int game_exit, char *player_word, char *word_to_guess);

#endif
